<?php get_header(); ?>
<?php get_template_part('template-parts/mainvisual'); ?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<section>
    <?php
    $facilit_area = get_the_terms(get_the_ID(), 'facility_area');
    foreach ($facilit_area as $area) {
        echo $area->name;
    }
    ?>

    <?php
    $facilit_class = get_the_terms(get_the_ID(), 'facility_class');
    foreach ($facilit_class as $class) {
        echo $class->name;
    }
    ?>


    <h2><?php the_title(); ?></h2>

    <?php if (get_field('facility_intro')) : ?>
        <p><?= get_field('facility_intro'); ?></p>
    <?php endif; ?>

    <?php if (get_field('facility_image')) : ?>
        <img src="<?= get_field('facility_image') ?>" alt="施設写真">
    <?php endif; ?>

    <div>
        <?php if (get_field('facility_address')) : ?>
            <div>
                <p>住所</p>
                <p><?= get_field('facility_address'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_phone')) : ?>
            <div>
                <p>電話番号</p>
                <p><?= get_field('facility_phone'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_fax')) : ?>
            <div>
                <p>FAX番号</p>
                <p><?= get_field('facility_fax'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_time')) : ?>
            <div>
                <p>保育時間</p>
                <p><?= get_field('facility_time'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_coretime')) : ?>
            <div>
                <p>コアタイム</p>
                <p><?= get_field('facility_coretime'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_capacity')) : ?>
            <div>
                <p>定員</p>
                <p><?= get_field('facility_capacity'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_age')) : ?>
            <div>
                <p>受け入れ年齢</p>
                <p><?= get_field('facility_age'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_class')) : ?>
            <div>
                <p>クラス編成</p>
                <p><?= get_field('facility_class'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_parking')) : ?>
            <div>
                <p>駐車場</p>
                <p><?= get_field('facility_parking'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_playground')) : ?>
            <div>
                <p>園庭</p>
                <p><?= get_field('facility_playground'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_park')) : ?>
            <div>
                <p>よく行く公園</p>
                <p><?= get_field('facility_park'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_futon')) : ?>
            <div>
                <p>布団持参</p>
                <p><?= get_field('facility_futon'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_diapers')) : ?>
            <div>
                <p>おむつ対応</p>
                <p><?= get_field('facility_diapers'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_event')) : ?>
            <div>
                <p>主な行事</p>
                <p><?= get_field('facility_event'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_parents')) : ?>
            <div>
                <p>保護者会</p>
                <p><?= get_field('facility_parents'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_community')) : ?>
            <div>
                <p>地域開放</p>
                <p><?= get_field('facility_community'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_fieldtrip')) : ?>
            <div>
                <p>見学について</p>
                <p><?= get_field('facility_fieldtrip'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_other')) : ?>
            <div>
                <p>その他の事業</p>
                <p><?= get_field('facility_other'); ?></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_homepage')) : ?>
            <div>
                <p>ホームページ</p>
                <p><?= get_field('facility_homepage'); ?></p>
            </div>
        <?php endif; ?>

    </div>

    <div>
        <?php if (get_field('facility_phone')) : ?>
            <div>
                <a href="tel:<?= get_field('facility_phone'); ?>"><?= get_field('facility_phone'); ?></a>
            </div>
        <?php endif; ?>

        <?php if (get_field('facility_homepage')) : ?>
            <div>
                <a href="<?= get_field('facility_homepage'); ?>">HPを見る</a>
            </div>
        <?php endif; ?>
    </div>

    <iframe src="https://maps.google.co.jp/maps?output=embed&q=<?= get_field('facility_address'); ?>&z=16" width="600" height="400" frameborder="0" scrolling="no"></iframe>



</section>


<?php get_footer(); ?>