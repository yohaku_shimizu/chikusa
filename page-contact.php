<?php get_header(); ?>
<?php get_template_part('template-parts/mainvisual'); ?>
<?php get_template_part('template-parts/breadcrumb'); ?>
<?php $theme_options = get_option('theme_option_name'); ?>

<section>
    <div class="page-width">
        <h2>ご質問やご相談を承ります。<br>どうぞお気軽にお問い合わせください。</h2>

        <div class="two-info">
            <div class="two-info__left">
                <p class="two-info__left--tell h2_text"><?php get_template_part('images/svg/green_phone'); ?><?= $theme_options['op_1']; ?></p>
                <p class="two-info__left--text"><?= $theme_options['op_2']; ?></p>
                <p class="two-info__left--text">受付時間<?= $theme_options['op_3']; ?></p>
            </div>
            <div class="two-info__right">
                <p class="two-info__right--tell h2_text"><?php get_template_part('images/svg/green_mail'); ?>メールフォーム</p>
                <p class="two-info__right--text">24時間受付中</p>
            </div>
        </div>
    </div>

</section>

<section>
    <h2>お問い合わせフォーム</h2>
    <p>入力欄は全て必須項目です。</p>
    <p>「個人情報保護方針」をお読みになり、同意のうえご記入ください。</p>
    <p>ご返信までに2～3日かかることもございますので、お急ぎの方はお電話にてお問い合わせください。</p>
    <p>返信メールをお受け取りいただけるよう、受信設定（迷惑メール設定）等をお確かめください。</p>
</section>

<?php get_footer(); ?>