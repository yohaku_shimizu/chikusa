<section class="mv-section">
    <?php if (is_home() || is_front_page()) : ?>
        <img src="<?php echo get_theme_mod('mainvisual'); ?>" alt="メインビジュアル">
        <p><?php echo get_theme_mod('mainvisual_text'); ?></p>
    <?php elseif (is_post_type_archive('facility_list')) : ?>
        <div class="under_green">
            <h1>施設一覧</h1>
        <?php elseif (is_single('facility')) : ?>
            <div class="under_green">
                <h1>施設情報</h1>
            </div>
        <?php elseif (is_page('about')) : ?>
            <div class="under_green">

                <h1>連絡会について</h1>
            </div>
        <?php elseif (is_archive('news')) : ?>
            <div class="under_green">
                <h1>お知らせ</h1>
            </div>
        <?php elseif (is_page('contact')) : ?>
            <div class="under_green">
                <h1>お問い合わせ</h1>
            </div>
        <?php endif; ?>
</section>