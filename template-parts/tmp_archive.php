<section>
    <div>

        <div>
            <?php $news_count = 1; ?>

            <?php if (have_posts()) : ?>
                <?php while (have_posts()) :  the_post() ?>
                    <!-- トップページのお知らせから遷移してきたときにアコーディオンをオープン状態にするための処理 -->
                    <!-- ゲットパラメータで送られてきたpost_numが入っている、かつ、パラメーターと$news_countが一致したときにopenクラス付与 -->
                    <?php if ($_GET['post_num'] &&  $_GET['post_num'] == $news_count || $news_count == 1 && !$_GET['post_num']) : ?>
                        <div class="accordion news open" id="news_count<?= $news_count ?>">
                        <?php else : ?>
                            <div class="accordion news" id="news_count<?= $news_count ?>">
                            <?php endif; ?>

                            <div>
                                <p><?= get_the_date(); ?></p>
                                <?php
                                $category = get_the_category();
                                echo $category[0]->cat_name;
                                ?>
                            </div>
                            <p class="h2_text"><?= get_the_title(); ?><?php get_template_part('images/svg/accordion-arrow'); ?></p>

                            </div>
                            <div class="accordion_child"><?php the_content(); ?></div>
                            <?php $news_count++; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                        </div>
                        <div>
                            <h2 class="accordion">カテゴリ―<?php get_template_part('images/svg/accordion-arrow-brown'); ?></h2>
                            <?php
                            // 親カテゴリーのものだけを一覧で取得
                            $args = array(
                                'parent' => 0,
                                'orderby' => 'term_order',
                                'order' => 'ASC',
                            );
                            $categories = get_categories($args);
                            ?>
                            <ul>
                                <?php foreach ($categories as $category) : ?>
                                    <li>
                                        <a href="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; ?>（<?php echo sprintf('%02d', $category->category_count); ?>）</a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>

                            <h2 class="accordion">アーカイブ<?php get_template_part('images/svg/accordion-arrow-brown'); ?></h2>
                            <ul>
                                <?php wp_get_archives('post_type=post&type=monthly&show_post_count=1'); ?>
                            </ul>
                        </div>
        </div>

        <?php
        /* ページャーの表示     */
        global $wp_query;
        if (function_exists('pagination')) :
            pagination($wp_query->max_num_pages, (get_query_var('paged')) ? get_query_var('paged') : 1);  //$wp_query ではなく $the_query ないことに注意！
        endif;
        ?>


</section>

<script>
    //accordion
    $(function() {
        $('.accordion').on('click', function() {
            /*クリックでコンテンツを開閉*/
            $(this).next().slideToggle(350);
            /*矢印の向きを変更*/
            $(this).toggleClass('open', 350);
        });
    });

    $(function() {
        // openクラスがあれば記事の内容を表示
        if ($('.accordion').hasClass('open')) {
            $('.accordion.open').next().css('display', 'block');
        }
    });
</script>