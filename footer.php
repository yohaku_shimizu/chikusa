<?php wp_footer(); ?>

<?php $theme_options = get_option('theme_option_name'); ?>

<footer>

    <div class="page-width">
        <div id="page_top">
            <p><?php get_template_part('images/svg/page-top') ?></p>
        </div>


        <img src="<?php echo get_theme_mod('logo_image'); ?>" alt="千種区保育園連絡会のロゴ">
        <p><?= $theme_options['op_1']; ?></p>
        <p><?= $theme_options['op_2']; ?></p>
        <p>受付時間<?= $theme_options['op_3']; ?></p>

        <p>メールフォーム</p>
        <p>24時間受付中</p>

        <nav>
            <ul>
                <li><a href="<?= home_url() ?>">トップページ</a></li>
                <li><a href="<?= home_url() ?>/facility_list">施設一覧</a></li>
                <li><a href="<?= home_url() ?>/about">連絡会について</a></li>
                <li><a href="<?= home_url() ?>/news">お知らせ</a></li>
            </ul>
        </nav>

        <p>© 2022 千種区保育園連絡会</p>
    </div>

</footer>

</body>

</html>