<?php get_header(); ?>
<?php get_template_part('template-parts/mainvisual'); ?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<section>
    <h2>千種区保育園連絡会とは</h2>
    <p><?= nl2br(get_field('about_sec_top_text')) ?></p>
    <img src="<?= get_field('about_sec_top_image') ?>" alt="子どもが走っている写真">
</section>

<section>
    <h2>事業の主な内容</h2>
    <div>
        <ul>
            <?php for ($i = 1; $i < 7; $i++) : ?>
                <li><?= get_field('about_sec_business_text_0' . $i) ?></li>
            <?php endfor; ?>
        </ul>
        <div>
            <img src="<?= get_field('about_sec_business_image') ?>" alt="子どもが遊んでいる写真">
        </div>
    </div>
</section>

<section>
    <h2>関連団体</h2>
    <?php
    $gallery_group = SCF::get('about_sec_group');
    foreach ($gallery_group as $fields) :
    ?>
        <a target="_blank" rel="noopener noreferrer" href="<?php echo $fields['about_sec_group_link']; ?>"><?php echo $fields['about_sec_group_text']; ?></a>
    <?php endforeach; ?>
</section>


<?php get_footer(); ?>