<?php get_header(); ?>
<?php get_template_part('template-parts/mainvisual'); ?>

<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<main>
    <section>
        <div class="page-width">
            <h2>エリアを選択してください。</h2>

            <div class="background-area">
                <?php get_template_part('images/svg/left-flower'); ?>
                <?php get_template_part('images/svg/map2') ?>
                <?php
                $terms = get_terms('facility_area');
                foreach ($terms as $term) : ?>
                    <a href="#<?= $term->name ?>"><?= $term->name ?></a>
                <?php endforeach; ?>


                <?php
                $taxonomy_name = 'facility_area';
                $taxonomys = get_terms($taxonomy_name);
                if (!is_wp_error($taxonomys) && count($taxonomys)) :
                    foreach ($taxonomys as $taxonomy) :
                        $tax_posts = get_posts(array(
                            'post_type' => get_post_type(), 'taxonomy' => $taxonomy_name,
                            'term' => $taxonomy->slug
                        ));
                        if ($tax_posts) :
                ?>
                            <p class="ht_text" id="<?php echo esc_html($taxonomy->name); ?>"><?php echo esc_html($taxonomy->name); ?></p>
                            <ul>
                                <?php
                                foreach ($tax_posts as $tax_post) :
                                ?>
                                    <li class="facility">
                                        <a href="<?php echo get_permalink($tax_post->ID); ?>">
                                            <h3><?php echo get_the_title($tax_post->ID); ?></h3>
                                        </a>
                                    </li>
                                <?php endforeach; ?>

                            </ul>
                <?php endif;
                    endforeach;
                endif; ?>

                <?php get_template_part('images/svg/right-weed'); ?>
            </div>
        </div>
    </section>

</main>


<?php get_footer(); ?>